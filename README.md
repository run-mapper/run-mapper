# Run-Mapper-Cordova

This is an example of a cordova application with a react single page application in its heart. It does not use react-native and is designed to provide a route for traditional react web applications to become native in their own right. Its accompanying react application can be found at [Run-Mapper-React](https://gitlab.com/run-mapper/run-mapper-react).

## How it works

The simplest way to use this repository is to first create a cordova application, as so:

```
cordova create run-mapper-cordova
```
Once this is created, go into the new directory, git init, and remove three files.

```
cd run-mapper-cordova
git init
rm config.xml
rm package.json
rm .npmignore
```

Finally, you associate this repository with your local cordova app's directory. Pull down the master branch, and you are ready to go.

```
git remote add origin git@gitlab.com:run-mapper/run-mapper.git
git pull origin master
```
## Adding React as a submodule

Next, we add run-mapper-react as a submodule to the cordova parent. This will put its contents in a directory of the same name (run-mapper-react). Once it has been added, cd into the directory, init the submodule, then update it.

```
git submodule add https://gitlab.com/run-mapper/run-mapper-react
cd run-mapper-react
git submodule init
git submodule update
```
The next thing that you do is run the react scripts (yarn build, specifically), then finally, you will target the individual platforms from this repository.

```
cordova platforms add ios
cordova run ios
```
## Troubleshooting

There are a number of moving pieces and places where things can go awry. Many of these are related to permissions from device vendors. For instance, you need a development team for building iOS applications. Also, there are things that do not apply or work on simulators that require actual devices to prove functionality.

This page will be updated with links to help you get around typical challenges to the development process.
